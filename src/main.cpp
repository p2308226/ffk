#include"MainMenu.h"
#include "Game.h"
#include<SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/Window.hpp>
#include<SFML/Audio.hpp>
#include<iostream>
#include<vector>
#include<stdio.h>
#include<stdlib.h>
#include<sstream>
#include<iomanip>
#include<random>
#include<ctime>
#include<time.h>
#include<string>

using namespace std;
using namespace sf;

int main()
{
    srand(time(0));
    //MAIN MENU
    RenderWindow window(VideoMode(1920, 1080), "Menu", Style::Default);
    MainMenu menuMenu (1920,1080);

    Texture menubg;
    menubg.loadFromFile("data/background.jpg");
    Sprite bg;
    bg.setTexture(menubg);

    //INSTRUCTIONS
    Texture instructions;
    instructions.loadFromFile("data/instructions.jpg");
    Sprite in;
    in.setTexture(instructions);


    /*SoundBuffer b;
    b.loadFromFile("//wsl.localhost/Ubuntu/home/firas/FFK/Game/assets/Menu/Medieval-Times.ogg");
    Sound s;
    s.setBuffer(b);*/

    //MUSIC
    Music m;
    m.openFromFile("data/sound/Medieval-Times.ogg");
    m.play();

    int x = -1;
    while (window.isOpen())
    {
        Event event;
        while (window.pollEvent(event))
        {
            if(event.type == Event::Closed)
            {
                window.close();
            }

            if(event.type == Event::KeyPressed)
            {
                if(event.key.code == Keyboard::Up){
                    menuMenu.MoveUp();
                }
                if(event.key.code == Keyboard::Down){
                    menuMenu.MoveDown();    
                }
                if (event.key.code == Keyboard::Return) {
                    x = menuMenu.pressed();

                }

                //L'index du boutton selectionné
                if (x == 0 )
                {   
                    Game game;
                    game.GameOn(window);
                    x = -1;
                    break;
                }

                if (x == 1 )
                {    
                    while (window.isOpen())
                    {
                        
                        Event aevent;
                        bool flag = 1;
                        while (window.pollEvent(aevent))
                        {
                            if(aevent.type == Event::Closed){
                                window.close();
                            }
                            if(aevent.type == Event::KeyPressed)
                            {
                                if(aevent.key.code == Keyboard::Escape){
                                    x = -1;
                                    flag = 0;
                                }
                            }
                        }
                        if (flag == 0) {
							break;
						}

                        window.clear();
                        window.draw(in);
                        window.display();
                        
                    }
                }

                if (x == 2){
                    window.close();
                }
            }
        }

        window.clear();
        window.draw(bg);
        menuMenu.draw(window);
        window.display();

    }

    return 0;
}