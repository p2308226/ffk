#include <iostream>
#include <string>

#include "Player.h"
#include "CardDeck.h"
#include "Card.h"



int Player::UpdateHealth(int amount) {// retourne le nouveau HP du joueur après les degats
    if(health!=0)
        this->health += amount;
    if (this->health > 100) {
        this->health = 100;
    } else if (this->health < 0) {
        this->health = 0;
    }
    return this->health;
}

int Player::GetHealth() const {// retourne les HP du joueur
	return this->health;
}

void Player::FillPlayerHand(CardDeck& deck){// remplir la main du joueur
    for (int i =0; i<5; i++){
        AddCardToHand(deck.GetRandomCardFromStack());
    }
}

void Player::AddCardToHand(const Card& card) {// ajoute une carte a la main du joueur
    for (int i = 0; i < 5; i++) {
        if (this->hand[i].IsEmpty()) {
            this->hand[i] = card;
            return;
        }
    }
    std::cout << "Impossible d’ajouter une autre carte" << std::endl;
}

Card Player::ChooseCard() {// choisir une carte au hazard pour jouer
    int index = rand() % 5;
    while(this->hand[index].IsEmpty() && index < 5) {
		index++;
        index%=5;
	}
    Card card = this->hand[index];
    hand[index].clear();
    return card;
}

Card Player::ChooseCard2(Vector2i mp) {// choisir une carte pour jouer
    Card card;
    if (mp.x >= 150 && mp.x <= 350)
    {
        card = hand[0];
        hand[0].clear();
    }
    else if (mp.x >= 400 && mp.x <= 600)
    {
        card = hand[1];
        hand[1].clear();
    }
    else if (mp.x >= 650 && mp.x <= 850)
    {
        card = hand[2];
        hand[2].clear();
    }
    else if (mp.x >= 900 && mp.x <= 1100)
    {
        card = hand[3];   
        hand[3].clear();
    }
    else if (mp.x >= 1150 && mp.x <= 1350)
    {
        card = hand[4];
		hand[4].clear();
    }
    return card;
}


void Player::drawCard(CardDeck& deck){// piocher une carte
    AddCardToHand(deck.GetRandomCardFromStack());
}

void Player::drawPlayerHand1(RenderWindow& window){//affiche la main du 1er Joueur

    Texture texture;

    for (int i = 0; i < 5; i++)
    {
        if (!this->hand[i].IsEmpty())
        {
			texture.loadFromFile(this->hand[i].image);
			Sprite sprite(texture);
			sprite.setPosition(150 + i*250, 650);
			window.draw(sprite);
		}
    }
}

string  Player::getCardDescription(Vector2i& mp){// affiche la main du 2eme Joueur
    if (mp.y >= 750 && mp.y <= 990)
    {
        if (mp.x >= 150 && mp.x <= 350)
        {
            return hand[0].description;
        }
        else if (mp.x >= 400 && mp.x <= 600)
        {
			return hand[1].description;
		}
        else if (mp.x >= 650 && mp.x <= 850)
        {
			return hand[2].description;
		}
        else if (mp.x >= 900 && mp.x <= 1100)
        {
			return hand[3].description;
		}
        else if (mp.x >= 1150 && mp.x <= 1350)
        {
			return hand[4].description;
		}
        else
        {
			return "";
		}
    }
    return "";
}

void Player::drawPlayerHand2(RenderWindow& window){//retourne la description d'une carte
    Texture texture;
    texture.loadFromFile("data/Cards/back.png");
    Sprite sprite(texture);

    for (int i = 0; i < 5; i++)
    {
        if (!this->hand[i].IsEmpty())
        {
            sprite.setPosition(165 + i * 250, 100);
            window.draw(sprite);
        }
    }
}
