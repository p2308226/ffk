#include <iostream>
#include <string>
#include <cstdlib> 
#include <ctime> 

#include "Card.h"
#include "CardDeck.h"

using namespace std;

//Tout les cartes du jeu
Card Crds[15] = {
    Card("Fire Dragon",-15,4,"Fire","Engloutit l adversaire dans des flammes brûlantes, lui infligeant 15 points de dégâts HP. Attention, son intensité peut être éteinte par des cartes de type Glace.","data/Cards/Fire_Dragon.png"),
    Card("Witch",-12,3,"Magic","Conjure un sort sinistre, infligeant 12 points de dégâts HP et empoisonnant l adversaire avec une réduction de -1 HP par seconde pendant 10 secondes. Sa magie malveillante ne connaît aucune faiblesse spécifique.","data/Cards/Witch.png"),
    Card("Ice Golem",-10,4,"Ice","Lance un froid glacial, infligeant 10 points de dégâts HP et immobilisant l adversaire pendant un tour. Sa prise glaciale peut être dégelée par des cartes de type Feu.","data/Cards/Ice_Golem.png"),
    Card("Samurai",-20,3,"Fighter","Exécute une technique de sabre magistrale, infligeant un coup dévastateur de 20 points de dégâts HP avec une précision experte.","data/Cards/Samurai.png"),
    Card("Knight",-16,3,"Fighter","Charge en avant avec vaillance, infligeant une puissante attaque d'épée qui inflige 16 points de dégâts HP à l adversaire.","data/Cards/Knight.png"),
    Card("Earthquake",-35,2,"Nature","Déclenche un bouleversement sismique dévastateur, causant 35 points de dégâts HP et brisant toutes les cartes sur le champ de bataille. De plus, l'adversaire reçoit 4 nouvelles cartes aléatoires.","data/Cards/Earthquake.png"),
    Card("The Great Wall",0,2,"Shield","Érige une barrière impénétrable, se protégeant contre toutes les attaques sauf la force imparable de la Météorite et du Tremblement de terre.","data/Cards/The_Great_Wall.png"),
    Card("Meteor",-40,1,"Nature","Invoque une force cataclysmique des cieux, infligeant un coup dévastateur de 40 points de dégâts HP à l impact.","data/Cards/Meteor.png"),
    Card("The Tree of Life",100,2,"Heal","Rayonne avec une énergie ancienne, accordant une restauration abondante de 100 points de HP, symbolisant le cycle éternel de la vie.","data/Cards/The_Tree_of_Life.png"),
    Card("Electro",-15,4,"Nature","Libère une vague d'électricité, choquant l adversaire avec 15 points de dégâts HP.","data/Cards/Electro.png"),
    Card("Healing Potion",40,5,"Heal","Administre un élixir puissant, restaurant 40 points de HP pour soigner les blessures et revitaliser le joueur.","data/Cards/Healing_Potion.png"),
    Card("Wolf",-15,3,"Fighter","Hurle dans la nuit, se précipitant pour infliger 15 points de dégâts HP tout en laissant l adversaire saigner à un taux de -1 HP par seconde pendant 10 secondes.","data/Cards/Wolf.png"),
    Card("Smoker",0,2,"Magic","Enveloppe le champ de bataille d'une brume dense, obscurcissant la vision de l adversaire pendant 2 tours. Cependant, elle peut être dissipée par des cartes de type Vent.","data/Cards/Smoker.png"),
    Card("The Dungeon Thief",0,3,"Not Known","Propose le choix entre un vol astucieux, saisissant la meilleure carte de l adversaire, ou un nouveau tirage dans le deck, offrant un avantage tactique ou un renouvellement stratégique.","data/Cards/The_Dungeon_Thief.png"),
    Card("Boost Potion",20,3,"Magic","Renforce le joueur, accordant +20 HP et augmentant les dégâts infligés de 13% pendant 2 tours, insufflant confiance et force dans la bataille.","data/Cards/Boost_Potio.png")
};

CardDeck::CardDeck() {//constructeur
	for (int i = 0; i < 15; i++) {
		this->CARDS[i] = Crds[i];
	}
}

void CardDeck::FillCardStack()
{//pour remplir la pioche
    int index = 0;
    for (int j = 0; j < 44; j++)
    {
        if (index < 44) {
            Stack[index] = CARDS[rand()%15];
            index++;
        } 
        else {
            break;
        }
    }
    //ShowAllGameCards();
}

Card CardDeck::GetRandomCardFromStack() {//une carte au hazard à partir de la pioche
    
    int index = rand() % 44;
    while (Stack[index].IsEmpty())
    {
        index=rand() % 44;
    }
	Card card = Stack[index];
    Stack[index].clear();
	return card;
}

bool CardDeck::IsEmpty() {//Verifie si la Pioche est vide
    bool flag = false;
    for(int i=0; i<44; i++){
		if(!Stack[i].IsEmpty()){
			flag = true;
		}
	}
    return flag;
}