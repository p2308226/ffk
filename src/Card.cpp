#include <iostream>
#include <string>
#include "Card.h"
#include <cstdlib> 
#include <ctime> 

using namespace std;

Card::Card(string name, int Damage, int number, string type, string description, string img) 
{//Constructeur
    this->name = name;
	this->Damage = Damage;
	this->number = number;
	this->type = type;
	this->description = description;
	this->image = img;
}

void Card::operator=(const Card& c)
{
	name = c.name;
	Damage = c.Damage;
	number = c.number;
	type = c.type;
	description = c.description;
	image = c.image;
}
bool Card::IsEmpty() const { //Verification si la carte est vide
    return name.empty();
}