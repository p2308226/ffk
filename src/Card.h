#pragma once
#ifndef CARD_H
#define CARD_H

#include <iostream>
#include <string>

using namespace std;


class Card {
    public :
        string name;    //nom de la carte
        int Damage;     //Damage de la carte
        int number;     //Le nombre de carte dans le jeu
        string type;    //le type de cette carte (exemple : Fire, Ice, Magic...)
        string description;     //Descripton de cette carte
        string image;   //Image de la carte

        //Constructeur
        Card() {};
        Card(string name, int Damage, int number, string type, string description, string img);

        void operator=(const Card& c);

        void clear()
        {//Retourne en défaut tous les attributs de la carte
			name = "";
			Damage = 0;
			number = 0;
			type = "";
			description = "";
            image = "";
        }
        bool IsEmpty()const; //Verification si la carte est vide
};

#endif