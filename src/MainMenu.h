#ifndef MAINMENU_H
#define MAINMENU_H

#include<SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/Window.hpp>
#include<SFML/Audio.hpp>

#include<iostream>

using namespace std;
using namespace sf;

class MainMenu{
    public:
        Text menu[3]; // Les boutons des menu
        MainMenu(float witdh, float height); // La fenêtre du menu
        void draw(RenderWindow& window)const; // fonction pour afficher la fenêtre du menu
        void MoveUp(); // Pour monter dans le menu
        void MoveDown(); // Pour descender dans le menu
        void setSelected(int n); // Retourner l'index bouton
        int pressed()const{ // Retourner le bouton sélectionné
            return selected;
        }
        ~ MainMenu(); // Constructeur

    private:
        int selected; //l'index
        Font font; // Police d'écriture
};

#endif