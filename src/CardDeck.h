#pragma once
#ifndef CARDDECK_H
#define CARDDECK_H

#include <iostream>
#include <string>
#include <cstdlib> 
#include <ctime> 

#include "Card.h"

using namespace std;

class CardDeck 
{
    public :
        Card CARDS[15]; // Tout les Cartes
        Card Stack[44]; // Le stack des Cartes

        CardDeck();//constructeur
        void FillCardStack();//pour remplir la pioche
        Card GetRandomCardFromStack();//une carte au hazard à partir de la pioche
        bool IsEmpty();//Verifie si la Pioche est vide
};

#endif 
