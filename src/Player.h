#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

#include "Card.h"
#include "Player.h"
#include "CardDeck.h"

using namespace std;
using namespace sf;


class Player {

    public:
        int health;
        Card hand[5];
        float boost;
    
        Player() { health = 0; boost = 1; };

        int UpdateHealth(int amount); // retourne le nouveau HP du joueur après les degats
        int GetHealth() const; // retourne les HP du joueur
        void FillPlayerHand(CardDeck& deck); // remplir la main du joueur
        void AddCardToHand(const Card& card); // ajoute une carte a la main du joueur
        Card ChooseCard(); // choisir une carte au hazard pour jouer
        Card ChooseCard2(Vector2i); // choisir une carte pour jouer
        void drawCard(CardDeck& deck); // piocher une carte
        void drawPlayerHand1(RenderWindow& window); //affiche la main du 1er Joueur
        void drawPlayerHand2(RenderWindow& window); // affiche la main du 2eme Joueur
        string getCardDescription(Vector2i&); //retourne la description d'une carte

};

#endif