#include "Game.h"
#include <iostream>
#include <string>
#include "Player.h"
#include "Card.h"
#include "CardDeck.h"
#include <sstream>

using namespace std;


void Game::FirstRound() {// Initialiser le jeu en commençant par distribuer les cartes dans les mains des joueurs
    
    player2.FillPlayerHand(deck);
    player1.FillPlayerHand(deck);
}

Card* Game::PlayRound(Vector2i mp) {// Jouer une Tour
    //POINTEUR
    Card card1;
    Card card2;

    card1 = player1.ChooseCard2(mp);
    card2 = player2.ChooseCard();

    Card* cards = new Card[2];
    cards[0] = card1;
    cards[1] = card2;
    return cards;
}

void Game::updateHealth(Card& card1, Card& card2){// Fonction pour la carte Boost Potion 
    float damage1 = card1.Damage * player1.boost;
    float damage2 = card2.Damage * player2.boost;

    if (damage1 > 0 && damage2 > 0)
    {
        player1.UpdateHealth(damage1);
        player2.UpdateHealth(damage2);
    }
    else if (damage2 > 0)
    {
        player2.UpdateHealth(damage1);
        player2.UpdateHealth(damage2);
    }
    else if (damage1 > 0)
    {
        player1.UpdateHealth(damage2);
        player1.UpdateHealth(damage1);
    }
    else
    {
        player1.UpdateHealth(damage2);
        player2.UpdateHealth(damage1);
    }
    if(player1.boost > 1)
        player1.boost = 1;
    if(player2.boost > 1)
		player2.boost = 1;
    if (card1.name == "Boost Potion")
		player1.boost = 1.3;
    if(card2.name == "Boost Potion")
        player2.boost = 1.3;
}

bool Game::IsGameOver() {// Verification si game over
    if (player1.GetHealth() <= 0) {
        return true;
    } else if (player2.GetHealth() <= 0) {
        return true;
    } else if (deck.IsEmpty()) {
        deck.FillCardStack();
        return false;
    } else {
        return false;
    }
}

void Game::justifyText(string& s){// Pour Justifier la description qui va être affiché à côté
    string temp;
	string result;
	istringstream iss(s);
    int size = 0;
    while (iss >> temp)
    {
        if (size + temp.size() >= 17)
        {
			result += "\n\n";
            size = 0;
		}
        size+= temp.size();
		result += temp + " ";
	}
	s = result;
}




// Fonction qui controle le Jeu

void Game::GameOn(RenderWindow& window) {// Fonction qui lance le jeu
    deck.FillCardStack();

    player1.health = 100;
    player2.health = 100;

    Font font;
    font.loadFromFile("data/font/pixelmix.ttf");

    //Texte de la description des cartes
    Text description;
    description.setFont(font);
    description.setCharacterSize(20);
    description.setFillColor(Color::Black);
    description.setPosition(1480, 250);
    string desc = "Cliquez avec le bouton droit de la souris pour obtenir des informations sur la carte. Cliquez avec le bouton gauche pour sélectionner la carte que vous souhaitez jouer. Appuyez sur Escape pour revenir au menu principal.\n";
    justifyText(desc);
    description.setString(desc);

    //HP du premier Joueur
    Text Health1;
    Health1.setFont(font);
    Health1.setCharacterSize(25);
    Health1.setFillColor(Color::White);
    Health1.setPosition(95, 920);

    //HP du deuxieme Joueur
    Text Health2;
    Health2.setFont(font);
    Health2.setCharacterSize(25);
    Health2.setFillColor(Color::White);
    Health2.setPosition(95, 10);

    //Bar pour Le HP des joueurs
    RectangleShape HealthBar1(Vector2f(12 * player1.GetHealth(), 50));
    RectangleShape HealthBar2(Vector2f(12 * player2.GetHealth(), 50));
    HealthBar1.setOutlineColor(Color::White);
    HealthBar2.setOutlineColor(Color::White);
    HealthBar1.setOutlineThickness(5);
    HealthBar2.setOutlineThickness(5);
    HealthBar1.setFillColor(Color::Green);
    HealthBar2.setFillColor(Color::Red);
    HealthBar1.setPosition(150, 920);
    HealthBar2.setPosition(150, 10);

    //Le background du jeu
    Texture gameBG;
    gameBG.loadFromFile("data/GameBg.png");
    Sprite bg1;
    bg1.setTexture(gameBG);

    //Pour afficher les details des cartes
    Texture box;
    box.loadFromFile("data/scroll.png");
    Sprite scroll;
    scroll.setTexture(box);
    scroll.setPosition(1400,75);
    


    FirstRound();// initialisation des mains des joueurs
    Card* cards = 0;
    bool flag = 1;
    Clock clock;
    bool roundflag = 0;
    while (window.isOpen() && !IsGameOver() && flag) {
        Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == Event::Closed) {
                window.close();
                return;
            }
            if (event.type == Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == Mouse::Left && !roundflag)
                {

					Vector2i mousePos = Mouse::getPosition(window);
                    if (mousePos.x >= 150 && mousePos.x <= 1350 && mousePos.y >= 750 && mousePos.y <= 990)
                    {
                        cards = PlayRound(mousePos);
                        clock.restart();
                        roundflag = 1;
                    }
				}
                if (event.mouseButton.button == Mouse::Right)
                {
                    Vector2i mousePos = Mouse::getPosition(window);
                    string descriptionText = player1.getCardDescription(mousePos);
                    justifyText(descriptionText);
                    if (descriptionText != "")
                    {
						description.setString(descriptionText);
					}
				}
			}
            if (event.type == Event::KeyPressed)
            {
                if (event.key.code == Keyboard::Escape)
                {
                    flag = 0;
                    break;
				}
			}
        }
        if (roundflag)
        {
            if (clock.getElapsedTime().asSeconds() > 4)
            {
                updateHealth(cards[0], cards[1]);
                player1.drawCard(deck);
                player2.drawCard(deck);
				roundflag = 0;
                delete[] cards;
                cards = 0;
			}
			
		}
        window.clear();
        window.draw(bg1);
        HealthBar1.setSize(Vector2f(12 * player1.GetHealth(), 50));
        HealthBar2.setSize(Vector2f(12 * player2.GetHealth(), 50));
        Health1.setString(to_string(player1.GetHealth()));
        Health2.setString(to_string(player2.GetHealth()));
        window.draw(HealthBar1);
        window.draw(HealthBar2);
        player1.drawPlayerHand1(window);
        player2.drawPlayerHand2(window);
        window.draw(scroll);
        window.draw(description);
        window.draw(Health1);
        window.draw(Health2);
        if (cards != 0)
        {
            RectangleShape rect(Vector2f(300, 300));
            //rect.setFillColor(Color::Red);
         //   rect.setPosition(800, 300);

            RectangleShape rect2(Vector2f(300, 300));
            //rect2.setFillColor(Color::Green);
          //  rect2.setPosition(500, 300);

            Texture texture1;
			Texture texture2;
		//	texture1.loadFromFile(cards[0].image);
		//	texture2.loadFromFile(cards[1].image);
		
			Sprite sprite1(texture1);
			Sprite sprite2(texture2);
			sprite1.setPosition(550, 350);
			sprite2.setPosition(850, 350);

    //            window.draw(rect);
  //          window.draw(rect2);
			window.draw(sprite1);
			window.draw(sprite2);
        }
        window.display();
    }
    if (player1.health > 0 && flag)
    {
        Clock clock;
        Sprite win;
        Texture winTexture;
        winTexture.loadFromFile("data/win.png");
        win.setTexture(winTexture);
        window.clear();
        window.draw(win);

        window.display();
        while (clock.getElapsedTime().asSeconds() <= 5);
        window.clear();
        return;
    }
}