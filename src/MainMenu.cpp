#include"MainMenu.h"
#include<SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/Window.hpp>
#include<SFML/Audio.hpp>
#include<iostream>
using namespace std;
using namespace sf;

MainMenu::MainMenu(float witdh, float height)
{
    font.loadFromFile("data/font/pixelmix.ttf");

    menu[0].setFont(font);
    menu[0].setFillColor(Color{255, 205, 0});
    menu[0].setString("Play");
    menu[0].setCharacterSize(70);
    menu[0].setPosition(Vector2f(400, 400));

    menu[1].setFont(font);
    menu[1].setFillColor(Color::White);
    menu[1].setString("Instructions");
    menu[1].setCharacterSize(70);
    menu[1].setPosition(Vector2f(400, 500));

    menu[2].setFont(font);
    menu[2].setFillColor(Color::White);
    menu[2].setString("Quitter");
    menu[2].setCharacterSize(70);
    menu[2].setPosition(Vector2f(400, 600));

    selected = 0;
}

MainMenu::~MainMenu(){};

void MainMenu::setSelected(int n){
    selected = n;
}

void MainMenu::draw(RenderWindow& window)const{
    for(int i=0; i<3; i++){
        window.draw(menu[i]);
    }
}

void MainMenu::MoveDown(){
    if (selected + 1 <= 3) //si je suis dans Quit
    {
        menu[selected].setFillColor(Color::White);
        selected++;
        if (selected == 3){
            selected = 0;
        }
        menu[selected].setFillColor(Color{255, 205, 0});
    }
}

void MainMenu::MoveUp(){
    if (selected - 1 >= -1) //si je suis dans Play
    {
        menu[selected].setFillColor(Color::White);
        selected--;
        if (selected == -1){
            selected = 2;
        }
        menu[selected].setFillColor(Color{255, 205, 0});
    }
}