#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <string>
#include "Player.h"
#include "Card.h"

using namespace std;

class Game {

    public:
        Player player1;
        Player player2;
        CardDeck deck;
        Game() {
        
        }
        void FirstRound(); // Initialiser le jeu en commençant par distribuer les cartes dans les mains des joueurs
        Card* PlayRound(Vector2i); // Jouer une Tour
        void updateHealth(Card& card1, Card& card2); // Fonction pour la carte Boost Potion 
        bool IsGameOver(); // Verification si game over
        void GameOn(RenderWindow&); // Fonction qui lance le jeu
        void justifyText(string&); // Pour Justifier la description qui va être affiché à côté

};

#endif