# FFK Cartes

## Informations du Projet
- **Nom**: FFK Cartes
- **Développeur**: LALLIAS Fabio ; ABDELMOULA Khalil ; KHADHRAOUI Firas
- **Numéros étudiants**: 12105194 ; 12205145 ; 12308226
- **Identifiant du Projet sur la Forge**: https://forge.univ-lyon1.fr/p2308226/ffk.git

---

## Manuel
### Compilation et Exécution
1. Clonez le projet sur votre machine.
2. Accédez au répertoire du projet.
3. Compilez et éxécutez le projet en utilisant la commande " make run ".

### Règles du Jeu
- **Mains de départ aléatoires**: Chaque joueur commence avec 4 cartes aléatoires, ajoutant un élément d'imprévisibilité.
- **Pioche aléatoire à chaque tour**: Les joueurs piochent une carte aléatoire à chaque tour.
- **Réponse avec une carte**: Les joueurs peuvent répondre à une attaque avec une carte de leur main.
- **Collection invisible pour l'adversaire**: Les collections de cartes de chaque joueur sont cachées à leur adversaire.
- **Capacités spéciales pour chaque carte**: Chaque carte a ses propres capacités spéciales.
- **Système d'énergie**: Les joueurs commencent avec 100 HP et utilisent cela comme un système d'énergie.
- **Médaille pour le vainqueur**: Le vainqueur reçoit une médaille.


---

## Organisation de l'Archive
L'archive est organisée de la manière suivante:
- **bin/**: Contient les fichiers éxécutables.
- **data/**: Contient les assets du projet (images, audio, police,...).
- **doc/**: Documentation du projet.
- **obj/**: Contient les fichiers compilés.
- **src/**: Contient les fichiers source du projet.
- **README.md**: Ce fichier.
- **Makefile**: Contient les commandes permettant la compilation et l'exécution du projet.

---
Amusez-vous bien avec FKK ! 🎮
