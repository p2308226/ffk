all: bin/sfml

bin/sfml: obj/main.o obj/MainMenu.o obj/Game.o obj/Player.o obj/CardDeck.o obj/Card.o
		mkdir -p bin
		g++ -o bin/sfml obj/main.o obj/MainMenu.o obj/Game.o obj/Player.o obj/CardDeck.o obj/Card.o -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system

obj/Card.o: src/Card.cpp src/Card.h
		g++ -g -Wall -c src/Card.cpp -o obj/Card.o

obj/CardDeck.o: src/CardDeck.cpp src/CardDeck.h src/Card.h
		g++ -g -Wall -c src/CardDeck.cpp -o obj/CardDeck.o

obj/Player.o: src/Player.cpp src/CardDeck.h src/Card.h
		g++ -g -Wall -c src/Player.cpp -o obj/Player.o

obj/Game.o: src/Game.cpp src/Game.h src/Player.h src/Card.h src/CardDeck.h
		g++ -g -Wall -c src/Game.cpp -o obj/Game.o

obj/MainMenu.o: src/MainMenu.cpp src/MainMenu.h
		g++ -g -Wall -c src/MainMenu.cpp -o obj/MainMenu.o

obj/main.o: src/main.cpp src/MainMenu.h src/Game.h src/Player.h
		g++ -g -Wall -c src/main.cpp -o obj/main.o


clean:
		rm obj/*.o

veryclean: clean
		rm *.out

documentation:
	firefox doc/html/index.html
	
doc: doc/doxyfile
  doxygen doc/doxyfile
	
run: bin/sfml
		./bin/sfml